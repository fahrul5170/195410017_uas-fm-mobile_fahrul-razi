<h1>Edit Product</h1>

<form action="/products/{{ $product->id }}" method="POST">
    @method('PUT')
    @csrf
    Nama : <input type="text" name="name" value="{{ $product->name }}"><br>
    Description : <input type="text" name="description"value="{{ $product->description }}"><br>
    Price : <input type="number" name="price"value="{{ $product->price }}"><br>
    Image URL : <input type="text" name="img_url" value="{{ $product->img_url }}"><br>

    <input type="submit" value="Save">
</form>