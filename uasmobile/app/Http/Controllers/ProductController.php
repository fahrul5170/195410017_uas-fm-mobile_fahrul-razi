<?php

namespace App\Http\Controllers;

use App\Models\Product;
use GuzzleHttp\Handler\Proxy;
use Illuminate\Http\Request;

class ProductController extends Controller{
        public function index() {
        $products = Product::all();
        return view('products.index', compact(['products']));
    }

    public function create()
    {
        # code...
        return view('products.create');
    }

    public function store(Request $request)
    {
        # code...
        Product::create($request->all());
        return redirect('/products');
    }

    public function edit($id)
    {
        # code...
        $product = Product::find($id);
        return view('products.edit', compact(['product']));
    }

    public function update(Request $request, $id)
    {
        # code...
        $product = Product::find($id);
        $product -> update($request->all());
        return redirect('/products');
    }

    public function distroy($id)
    {
        # code...
        $product = Product::find($id);
        $product->delete();
        return redirect('/products');
    }
}
