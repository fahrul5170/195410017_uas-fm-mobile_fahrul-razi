import 'package:flutter/material.dart';
import 'package:tokopedia/screens/homepage.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Rollo Store',
      home: HomePage(),
    );
  }
}

