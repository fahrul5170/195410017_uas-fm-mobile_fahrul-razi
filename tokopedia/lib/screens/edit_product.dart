import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:tokopedia/screens/homepage.dart';
import 'package:http/http.dart' as http;

// ignore: must_be_immutable
class EditProduct extends StatelessWidget {
  // EditProduct({Key? key}) : super(key: key);

  final Map product;
  EditProduct({Key? key, required this.product}) : super(key: key);

  final _formKey = GlobalKey<FormState>();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _priceController = TextEditingController();
  final TextEditingController _imgUrlController = TextEditingController();
  Future updateProduct() async {
    // ignore: unused_local_variable
    var response = await http.put(
        Uri.parse(
            "http://10.0.2.2:8000/api/products" + product['id'].toString()),
        body: {
          "name": _nameController.text,
          "description": _descriptionController.text,
          "price": _priceController.text,
          "img_url": _imgUrlController.text
        });
    var encodeFirts = json.encode(response.body);
    return json.decode(encodeFirts);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Edit Product"),
      ),
      body: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                TextFormField(
                  controller: _nameController..text = product['name'],
                  decoration: const InputDecoration(labelText: "Nama"),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Mohon isi nama produk";
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _descriptionController
                    ..text = product['description'],
                  decoration: const InputDecoration(labelText: "Description"),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Mohon isi Deskripsi";
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _priceController..text = product['price'],
                  decoration: const InputDecoration(labelText: "Price"),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Mohon isi harga produk";
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _imgUrlController..text = product['img_url'],
                  decoration: const InputDecoration(labelText: "Image URL"),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Mohon isi alamat gambar produk";
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        updateProduct().then((value) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const HomePage()));
                        });
                      }
                    },
                    child: const Text("Update Product"))
              ],
            ),
          )),
    );
  }
}
