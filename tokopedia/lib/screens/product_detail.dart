import 'package:flutter/material.dart';

class ProductDetail extends StatelessWidget {
  final Map product;

  // ignore: use_key_in_widget_constructors
  const ProductDetail({required this.product});

  @override
  Widget build(BuildContext context) {
    // ignore: avoid_unnecessary_containers
    return Scaffold(
      appBar: AppBar(
        title: const Text('Product Detail')
      ),
      body: Column(
        children: [
          // ignore: avoid_unnecessary_containers
          Padding(
            padding: const EdgeInsets.all(8.0),
            // ignore: avoid_unnecessary_containers
            child: Container(
              child: Image.network(product['img_url']),
            ),
          ),
          const SizedBox(
            height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(product['price'],style: const TextStyle(fontSize: 22),
                  ),
                  Row(
                    children: const [
                      Icon(Icons.edit),
                      Icon(Icons.delete)
                    ],
                  )
                ],
              ),
            ),
            Text(product['description'])
        ]),
    );
  }
}