import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:tokopedia/screens/homepage.dart';
import 'package:http/http.dart' as http;

// ignore: must_be_immutable
class AddProduct extends StatelessWidget {
  AddProduct({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormState>();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _priceController = TextEditingController();
  final TextEditingController _imgUrlController = TextEditingController();
  Future saveProduct() async {
    // ignore: unused_local_variable
    final response =
        await http.post(Uri.parse("http://10.0.2.2:8000/api/products"), body: {
      "name": _nameController.text,
      "description": _descriptionController.text,
      "price": _priceController.text,
      "img_url": _imgUrlController.text
    });

    return json.decode(response.body);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add Product"),
      ),
      body: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                TextFormField(
                  controller: _nameController,
                  decoration: const InputDecoration(labelText: "Nama"),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Mohon isi nama produk";
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _descriptionController,
                  decoration: const InputDecoration(labelText: "Description"),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Mohon isi Deskripsi";
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _priceController,
                  decoration: const InputDecoration(labelText: "Price"),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Mohon isi harga produk";
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _imgUrlController,
                  decoration: const InputDecoration(labelText: "Image URL"),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Mohon isi alamat gambar produk";
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        saveProduct().then((value) {
                          Navigator.push(context,
                              MaterialPageRoute(
                                builder: (context) => const HomePage()
                               )
                            );
                        });
                      }
                    },
                    child: const Text("Save"))
              ],
            ),
          )),
    );
  }
}
